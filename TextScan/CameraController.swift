//
//  CameraController.swift
//  TextScan
//
//  Created by Lukas Klehr on 09.06.22.
//  Reference: Pranjal Satija, 29.05.2017, https://github.com/appcoda/FullScreenCamera
//  Edited by Lukas Klehr

import UIKit
import AVFoundation

class CameraController: NSObject, AVCaptureVideoDataOutputSampleBufferDelegate {
    var captureSession: AVCaptureSession?
    
    var currentCameraPosition: CameraPosition?
    
    var frontCamera: AVCaptureDevice?
    var frontCameraInput: AVCaptureDeviceInput?
    
    var photoOutput: AVCapturePhotoOutput?
    
    var rearCamera: AVCaptureDevice?
    var rearCameraInput: AVCaptureDeviceInput?
    
    var previewLayer: AVCaptureVideoPreviewLayer?
    
    var flashMode = AVCaptureDevice.FlashMode.off
    var photoCaptureCompletionBlock: ((UIImage?, Error?) -> Void)?
    
    var sampleBufferForProcessing: CMSampleBuffer?
    
    // Lukas Klehr 09.06.2022
    var videoOutput: AVCaptureVideoDataOutput?
}

extension CameraController {
    
    // Preparing the camera controller
    // Reference: Pranjal Satija, 29.05.2017, https://github.com/appcoda/FullScreenCamera
    func prepare(completionHandler: @escaping (Error?) -> Void) {
        
        // Creating a capture session
        // Reference: Pranjal Satija, 29.05.2017, https://github.com/appcoda/FullScreenCamera
        func createCaptureSession() {
            self.captureSession = AVCaptureSession()
        }
        
        // Configuring the capture devices: back and front camera
        // Reference: Pranjal Satija, 29.05.2017, https://github.com/appcoda/FullScreenCamera
        func configureCaptureDevices() throws {
            
            let session = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInDualCamera], mediaType: AVMediaType.video, position: .unspecified)
            
            let cameras = session.devices.compactMap { $0 }
            guard !cameras.isEmpty else { throw CameraControllerError.noCamerasAvailable }
            
            for camera in cameras {
                if camera.position == .front {
                    self.frontCamera = camera
                }
                
                if camera.position == .back {
                    self.rearCamera = camera
                    
                    try camera.lockForConfiguration()
                    camera.focusMode = .continuousAutoFocus
                    camera.unlockForConfiguration()
                }
            }
        }
        
        // Configuring the device inputs: inputs coming from the camera
        // Reference: Pranjal Satija, 29.05.2017, https://github.com/appcoda/FullScreenCamera
        func configureDeviceInputs() throws {
            guard let captureSession = self.captureSession else { throw CameraControllerError.captureSessionIsMissing }
            
            if let rearCamera = self.rearCamera {
                self.rearCameraInput = try AVCaptureDeviceInput(device: rearCamera)
                
                if captureSession.canAddInput(self.rearCameraInput!) { captureSession.addInput(self.rearCameraInput!) }
                
                self.currentCameraPosition = .rear
            }
                
            else if let frontCamera = self.frontCamera {
                self.frontCameraInput = try AVCaptureDeviceInput(device: frontCamera)
                
                if captureSession.canAddInput(self.frontCameraInput!) { captureSession.addInput(self.frontCameraInput!) }
                else { throw CameraControllerError.inputsAreInvalid }
                
                self.currentCameraPosition = .front
            }
                
            else { throw CameraControllerError.noCamerasAvailable }
        }
        
        // Configuring the device outputs: outputs used for processing
        // Reference: Pranjal Satija, 29.05.2017, https://github.com/appcoda/FullScreenCamera
        func configurePhotoOutput() throws {
            guard let captureSession = self.captureSession else { throw CameraControllerError.captureSessionIsMissing }
            
            self.photoOutput = AVCapturePhotoOutput()
            self.photoOutput!.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format: [AVVideoCodecKey : AVVideoCodecType.jpeg])], completionHandler: nil)
            
            self.videoOutput = AVCaptureVideoDataOutput()
            self.videoOutput!.videoSettings = [(kCVPixelBufferPixelFormatTypeKey as NSString) : NSNumber(value: kCVPixelFormatType_32BGRA)] as [String: Any]
            self.videoOutput!.setSampleBufferDelegate(self, queue: DispatchQueue(label: "my.image.handling.queue"))
            
            if captureSession.canAddOutput(self.videoOutput!) { captureSession.addOutput(self.videoOutput!) }
            
            if captureSession.canAddOutput(self.photoOutput!) { captureSession.addOutput(self.photoOutput!) }
            captureSession.startRunning()
        }
        
        // Error handling if something goes wrong with the camera configuration
        // Reference: Pranjal Satija, 29.05.2017, https://github.com/appcoda/FullScreenCamera
        DispatchQueue(label: "prepare").async {
            do {
                createCaptureSession()
                try configureCaptureDevices()
                try configureDeviceInputs()
                try configurePhotoOutput()
            }
                
            catch {
                DispatchQueue.main.async {
                    completionHandler(error)
                }
                
                return
            }
            
            DispatchQueue.main.async {
                completionHandler(nil)
            }
        }
    }
    
    // Configuring the video preview shown for the live preview
    // Reference: Pranjal Satija, 29.05.2017, https://github.com/appcoda/FullScreenCamera
    func displayPreview(on view: UIView) throws {
        guard let captureSession = self.captureSession, captureSession.isRunning else { throw CameraControllerError.captureSessionIsMissing }
        
        self.previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        self.previewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        self.previewLayer?.connection?.videoOrientation = .portrait
        
        view.layer.insertSublayer(self.previewLayer!, at: 0)
        self.previewLayer?.frame = view.frame
    }
    
    // Capturing the image: Creating a still UIImage which is used for processing
    // Reference: Pranjal Satija, 29.05.2017, https://github.com/appcoda/FullScreenCamera
    func captureImage(completion: @escaping (UIImage?, Error?) -> Void) {
        guard let captureSession = captureSession, captureSession.isRunning else { completion(nil, CameraControllerError.captureSessionIsMissing); return }
        
        let settings = AVCapturePhotoSettings()
        settings.flashMode = self.flashMode
        
        self.photoOutput?.capturePhoto(with: settings, delegate: self)
        self.photoCaptureCompletionBlock = completion
    }

}

extension CameraController: AVCapturePhotoCaptureDelegate {
    
    // Configuring the photo output
    // Reference: Pranjal Satija, 29.05.2017, https://github.com/appcoda/FullScreenCamera
    public func photoOutput(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?,
                        resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Swift.Error?) {
        if let error = error { self.photoCaptureCompletionBlock?(nil, error) }
            
        else if let buffer = photoSampleBuffer, let data = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: buffer, previewPhotoSampleBuffer: nil),
            let image = UIImage(data: data) {
            
            self.photoCaptureCompletionBlock?(image, nil)
        }
            
        else {
            self.photoCaptureCompletionBlock?(nil, CameraControllerError.unknown)
        }
    }
    
    // Configuring the capture output to create a sample buffer for live processing
    // Lukas Klehr
    public func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        
        sampleBufferForProcessing = sampleBuffer
    }
}

extension CameraController {
    
    // Defining the errors for the camera controller
    // Reference: Pranjal Satija, 29.05.2017, https://github.com/appcoda/FullScreenCamera
    enum CameraControllerError: Swift.Error {
        case captureSessionAlreadyRunning
        case captureSessionIsMissing
        case inputsAreInvalid
        case invalidOperation
        case noCamerasAvailable
        case unknown
    }
    
    // Defining the positions of the camera
    // Reference: Pranjal Satija, 29.05.2017, https://github.com/appcoda/FullScreenCamera
    public enum CameraPosition {
        case front
        case rear
    }
}

