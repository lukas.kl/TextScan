//
//  Copyright (c) 2020 Google Inc.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import AVFoundation
import MLKitVision
import UIKit

// Defines UI-related utilitiy methods for vision detection.
// Reference: Google Inc., 06.08.2021, URL - https://codeload.github.com/googlecodelabs/mlkit-ios/zip/refs/heads/master
public class UIUtilities {
    
    // MARK: - Public
    
    // Adding a rectange based on a CGRect rectangle and adding it to the submitted UIView
    // Reference: Google Inc., 06.08.2021, URL - https://codeload.github.com/googlecodelabs/mlkit-ios/zip/refs/heads/master
    public static func addRectangle(_ rectangle: CGRect, to view: UIView, color: UIColor) {
        let rectangleView = UIView(frame: rectangle)
        rectangleView.layer.cornerRadius = Constants.rectangleViewCornerRadius
        rectangleView.alpha = Constants.rectangleViewAlpha
        rectangleView.backgroundColor = color
        view.addSubview(rectangleView)
    }
    
    // Following uncommented functions would be necesssary for the live processing of the video preview

//  public static func imageOrientation(
//    fromDevicePosition devicePosition: AVCaptureDevice.Position = .back
//  ) -> UIImage.Orientation {
//    var deviceOrientation = UIDevice.current.orientation
//    if deviceOrientation == .faceDown || deviceOrientation == .faceUp
//      || deviceOrientation == .unknown
//    {
//      deviceOrientation = currentUIOrientation()
//    }
//    switch deviceOrientation {
//    case .portrait:
//      return devicePosition == .front ? .leftMirrored : .right
//    case .landscapeLeft:
//      return devicePosition == .front ? .downMirrored : .up
//    case .portraitUpsideDown:
//      return devicePosition == .front ? .rightMirrored : .left
//    case .landscapeRight:
//      return devicePosition == .front ? .upMirrored : .down
//    case .faceDown, .faceUp, .unknown:
//      return .up
//    @unknown default:
//      return .up
//    }
//  }

  // MARK: - Private

//  private static func currentUIOrientation() -> UIDeviceOrientation {
//    let deviceOrientation = { () -> UIDeviceOrientation in
//      switch UIApplication.shared.statusBarOrientation {
//      case .landscapeLeft:
//        return .landscapeRight
//      case .landscapeRight:
//        return .landscapeLeft
//      case .portraitUpsideDown:
//        return .portraitUpsideDown
//      case .portrait, .unknown:
//        return .portrait
//      @unknown default:
//        return .portrait
//      }
//    }
//    guard Thread.isMainThread else {
//      var currentOrientation: UIDeviceOrientation = .portrait
//      DispatchQueue.main.sync {
//        currentOrientation = deviceOrientation()
//      }
//      return currentOrientation
//    }
//    return deviceOrientation()
//  }
}

// MARK: - Constants

// Defining the necessary constants
// Reference: Google Inc., 06.08.2021, URL - https://codeload.github.com/googlecodelabs/mlkit-ios/zip/refs/heads/master
private enum Constants {
    static let rectangleViewAlpha: CGFloat = 0.3
    static let rectangleViewCornerRadius: CGFloat = 10.0
}
