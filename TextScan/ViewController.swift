//
//  ViewController.swift
//  TextScan
//
//  Created by Lukas Klehr on 07.06.22.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    let cameraController = CameraController()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configuring the camera controller
        // Reference: Pranjal Satija, 29.05.2017, URL - https://github.com/appcoda/FullScreenCamera ->
        func configureCameraController() {
            cameraController.prepare {(error) in
                if let error = error {
                    print(error)
                }
                
                try? self.cameraController.displayPreview(on: self.view)
            }
        }
        
        configureCameraController()
    }
    
    // Forcing the app to stay in portrait mode and prohibit the autorotation
    // Reference: Ai-Lyn Tang, 26.11.2019, URL - https://code.likeagirl.io/xcode-how-to-force-a-view-into-landscape-mode-in-ios-13-15340978e3f4
    override public var shouldAutorotate: Bool {
        return false
    }
    override public var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    override public var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    // MARK: Actions
    
    // Lukas Klehr: 09.06.2022
    @IBAction func recordButtonDidTouch(_ sender: UIButton) {
        cameraController.captureImage { (image, error) in
            guard let image = image else {
                print(error ?? "Image capture error")
                return
            }
            
            ImageController.receivedImage = image
            self.performSegue(withIdentifier: "imageTransition", sender: self)
        }
    }
}
