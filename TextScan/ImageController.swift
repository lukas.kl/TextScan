//
//  ImageController.swift
//  TextScan
//
//  Created by Lukas Klehr on 09.06.22.
//

import UIKit
import MLKit
import MLKitVision
import MLKitTextRecognition

class ImageController: UIViewController {
    
    // Configuring the outlets connected to the storyboard
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var textImageButton: UIButton!
    
    // Static variable for transfer of the image (View Controller -> Image Controller)
    static var receivedImage: UIImage!
    
    var isImage: Bool = true
    var recognizedText: String!
    
    // Definition of the Images used on the result view
    let imageButtonImage = UIImage(systemName: "doc.text.magnifyingglass")
    let textButtonImage = UIImage(systemName: "doc.richtext")
    
    // An overlay view that displays detection annotations.
    // Reference: Google Inc., 06.08.2021, URL - https://codeload.github.com/googlecodelabs/mlkit-ios/zip/refs/heads/master
    private lazy var annotationOverlayView: UIView = {
        precondition(isViewLoaded)
        let annotationOverlayView = UIView(frame: .zero)
        annotationOverlayView.translatesAutoresizingMaskIntoConstraints = false
        return annotationOverlayView
    }()
    
    // Reference: Google Inc., 06.08.2021, URL - https://codeload.github.com/googlecodelabs/mlkit-ios/zip/refs/heads/master
    private lazy var resultsAlertController: UIAlertController = {
        let alertController = UIAlertController(
            title: "Detection Results",
            message: nil,
            preferredStyle: .actionSheet)
        alertController.addAction(
            UIAlertAction(title: "OK", style: .destructive) { _ in
                alertController.dismiss(animated: true, completion: nil)
            })
        return alertController
    }()
    
    // Defining the options for and creating an instance of the text recognizer
    private let latinOptions = TextRecognizerOptions()
    private lazy var textRecognizer = TextRecognizer.textRecognizer(options:latinOptions)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.isHidden = true
        
        imageView.image = ImageController.receivedImage
        
        // Reference: Google Inc., 06.08.2021, URL - https://codeload.github.com/googlecodelabs/mlkit-ios/zip/refs/heads/master
        imageView.addSubview(annotationOverlayView)
        NSLayoutConstraint.activate([
            annotationOverlayView.topAnchor.constraint(equalTo: imageView.topAnchor),
            annotationOverlayView.leadingAnchor.constraint(equalTo: imageView.leadingAnchor),
            annotationOverlayView.trailingAnchor.constraint(equalTo: imageView.trailingAnchor),
            annotationOverlayView.bottomAnchor.constraint(equalTo: imageView.bottomAnchor),
        ])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        runTextRecognition(with: imageView.image!)
    }
    
    // MARK: Actions
    
    @IBAction func showTextButtonDidTouch(_ sender: UIButton) {
        imageView.isHidden.toggle()
        scrollView.isHidden.toggle()
        
        isImage.toggle()
        
        if isImage {
            textImageButton.setImage(imageButtonImage, for: .normal)
        }
        else {
            textImageButton.setImage(textButtonImage, for: .normal)
        }
    }
    
    // MARK: Text Recognition
    
    // Running the text recognition
    // Reference: Google Inc., 06.08.2021, URL - https://codeload.github.com/googlecodelabs/mlkit-ios/zip/refs/heads/master
    // Edited by Lukas Klehr on 03.08.2022
    func runTextRecognition(with image: UIImage) {
        let visionImage = VisionImage(image: image)
        // Setting the correct image orientation
        // Reference: Google ML Kit - Text Recognizer v2, 07.06.2022 , URL - https://developers.google.com/ml-kit/vision/text-recognition/v2/ios
        visionImage.orientation = ImageController.receivedImage.imageOrientation
        textRecognizer.process(visionImage) { features, error in
            self.processResult(from: features, error: error)
        }
    }
    
//    func runTextRecognitionLive(with sampleBuffer: CMSampleBuffer) {
//        let visionImage = VisionImage(buffer: sampleBuffer)
//        textRecognizer.process(visionImage) { features, error in
//            self.processResult(from: features, error: error)
//        }
//    }
    
    // Processing the results and defining the bounding boxes and the resulting text label
    // Reference: Google Inc., 06.08.2021, URL - https://codeload.github.com/googlecodelabs/mlkit-ios/zip/refs/heads/master
    func processResult(from text: Text?, error: Error?) {
        removeDetectionAnnotations()
        guard error == nil, let text = text else {
            let errorString = error?.localizedDescription ?? Constants.detectionNoResultsMessage
            print("Text recognizer failed with error: \(errorString)")
            return
        }

        let transform = self.transformMatrix()
        
        recognizedText = "The Detection Results are: \n"
        
        // Iterating through the detected text
        // Blocks.
        for block in text.blocks {
            // Transforming the block frames and drawing the bounding boxes
            let blockframe = transformBoundingBoxes(block.frame)
            drawFrame(blockframe, in: .purple, transform: transform)
            
            // Lines.
            for line in block.lines {
                // Transforming the line frames and drawing the bounding boxes
                let lineframe = transformBoundingBoxes(line.frame)
                drawFrame(lineframe, in: .orange, transform: transform)
                
                // Elements.
                for element in line.elements {
                    // Transforming the element frames and drawing the bounding boxes
                    let elementframe = transformBoundingBoxes(element.frame)
                    drawFrame(elementframe, in: .green, transform: transform)
                    
                    // Adding the label text for the elements (words)
                    let transformedRect = elementframe.applying(transform)
                    let label = UILabel(frame: transformedRect)
                    label.text = element.text
                    label.adjustsFontSizeToFitWidth = true
                    self.annotationOverlayView.addSubview(label)
                    
                    // Adding elements / words and a blank space to the recognizedText variable
                    recognizedText = recognizedText + element.text
                    recognizedText = recognizedText + " "
                }
                
                // Adding line break to the recognizedText variable to indicate a new line
                recognizedText = recognizedText + "\n"
            }
            
            // Adding two line breaks to the recognizedText variable to indicate a new block
            recognizedText = recognizedText + "\n"
        }
        // Adding the recognized text to the UILabel text to show the text on the screen
        if recognizedText == "The Detection Results are: \n" {
            resultLabel.text = "No text found!"
        }
        else {
            resultLabel.text = recognizedText
        }
    }
    
    // Transforming the bounding boxes from the ML Kit to fit the application
    private func transformBoundingBoxes(_ frame: CGRect) -> CGRect {
        let transformedBoundingBox = CGRect(x: 1080 - frame.minY, y: frame.minX, width: -frame.height, height: frame.width)
        return transformedBoundingBox
    }
    
    // Drawing the frame for the bounding boxes
    // Reference: Google Inc., 06.08.2021, URL - https://codeload.github.com/googlecodelabs/mlkit-ios/zip/refs/heads/master
    private func drawFrame(_ frame: CGRect, in color: UIColor, transform: CGAffineTransform) {
        let transformedRect = frame.applying(transform)
        UIUtilities.addRectangle(
            transformedRect,
            to: self.annotationOverlayView,
            color: color
        )
    }
    
    // Defining a Transformation between the image view size and the image size
    // Reference: Google Inc., 06.08.2021, URL - https://codeload.github.com/googlecodelabs/mlkit-ios/zip/refs/heads/master
    private func transformMatrix() -> CGAffineTransform {
        guard let image = imageView!.image else { return CGAffineTransform() }
        let imageViewWidth = imageView!.frame.size.width
        let imageViewHeight = imageView!.frame.size.height
        let imageWidth = image.size.width
        let imageHeight = image.size.height
        
        let imageViewAspectRatio = imageViewWidth / imageViewHeight
        let imageAspectRatio = imageWidth / imageHeight
        let scale =
            (imageViewAspectRatio > imageAspectRatio)
            ? imageViewHeight / imageHeight : imageViewWidth / imageWidth
        
        // Image view's `contentMode` is `scaleAspectFit`, which scales the image to fit the size of the
        // image view by maintaining the aspect ratio. Multiple by `scale` to get image's original size.
        let scaledImageWidth = imageWidth * scale
        let scaledImageHeight = imageHeight * scale
        let xValue = (imageViewWidth - scaledImageWidth) / CGFloat(2.0)
        let yValue = (imageViewHeight - scaledImageHeight) / CGFloat(2.0)
        
        var transform = CGAffineTransform.identity.translatedBy(x: xValue, y: yValue)
        transform = transform.scaledBy(x: scale, y: scale)
        return transform
    }

    // Removes the detection annotations from the annotation overlay view.
    // Reference: Google Inc., 06.08.2021, URL - https://codeload.github.com/googlecodelabs/mlkit-ios/zip/refs/heads/master
    private func removeDetectionAnnotations() {
        for annotationView in annotationOverlayView.subviews {
            annotationView.removeFromSuperview()
        }
    }
}

// MARK: - Fileprivate

// Copyright by Google: original source code available under https://codeload.github.com/googlecodelabs/mlkit-ios/zip/refs/heads/master
// Edited by Lukas Klehr
fileprivate enum Constants {
    static let detectionNoResultsMessage = "No results returned."
}
